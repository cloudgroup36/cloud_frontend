var map, heatmap;


/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
//////////////// load map////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
var melbourne = {lat:-37.8136, lng: 144.9631};
var adelaide = {lat: -34.9285, lng: 138.6007};
var brisbane ={lat: -27.4698, lng:153.0251};
var australia = {lat:-25.2744, lng:133.7751};

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: australia,
    zoom: 4,
    styles: 
      [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
  });
          
//    heatmap = new google.maps.visualization.HeatmapLayer({
//          data: getPoints(),
//          map: map,
//        radius:50
//        });

}



/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
//////////////// fucntions for Map suburbs///////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////



function drawSuburb(coordinates, suburbName){
   var suburbPolygon = new google.maps.Polygon({
       paths:coordinates,
       strokeColor: '#000000',
       strokeOpacity: 1,
       strokeWeight: 0.1,
       fillColor: '#EFD4AE',
       fillOpacity:0.1
   });
    
//    var ring = new google.maps.Data.LinearRing(coordinates);

    
//FILL AREA OF LOCATIONS THAT IS INCLUDED
    for(var i in diabetes_aus.ratio){
        if(diabetes_aus.ratio[i].name===suburbName){
            if(diabetes_aus.ratio[i].diabetes!=null){
                        suburbPolygon.setOptions({fillColor: fillGraph(diabetes_aus.ratio[i].diabetes),
                        fillOpacity:1});
            }
        }
    }
    
  suburbPolygon.setMap(map);

}
      

function fillGraph(ratio){
    if(ratio >= 27.919 && ratio <32.282){
        return '#fffccc';
    }else if(ratio>=32.282 && ratio <34.286){
        return '#fff37f';
    }else if(ratio>=34.286 && ratio <36.407){
        return '#FFCE00';
    }else if(ratio>=36.407 && ratio<=40.174 ){
        return '#FFB400';
    }
}

function drawData(){
    for(var i = 49000; i<56000;i++){
        var position = {lat:au_location.coordinates[i][1], lng:au_location.coordinates[i][0]};
        var circle = new google.maps.Circle({
                strokeColor: '#FF0000',
               strokeOpacity: 0,
               strokeWeight: 0.1,
               fillColor: '#FF0000',
               fillOpacity: 0.3,
               map: map,
               center: position,
               radius: 20000
        });
    }
}

//function getPoints(){
//    var points = [];
//    for (var i = 0; i<au_location.coordinates.length;i++){
//        var position = new google.maps.LatLng(au_location.coordinates[i][1],au_location.coordinates[i][0]);
//        points.push(position);
//    }
//    
//    return points;
//}
//
//function toggleHeatmap() {
//        heatmap.setMap(heatmap.getMap() ? null : map);
//}


var diabetes_aus = {};
var lga_aus = [];
$(document).ready(function(){
//    drawData(49000);
//    drawData(100000);
//////////////////////////////
//DRAW BOUNDARIES FOR MAP/////
////////////////////////////       
//     $.each(au_location.coordinates, function(i,coordinate){
//          
//         var position = {lat:coordinate[1], lng:coordinate[0]};
//             var cityCircle = new google.maps.Circle({
//               strokeColor: '#FF0000',
//               strokeOpacity: 0,
//               strokeWeight: 0.1,
//               fillColor: '#3399FF',
//               fillOpacity: 0.35,
//               map: map,
//               center: position,
//               radius: 3000
//         });
//     });    

//
//
     $.each(diabetes_au.features, function(i, feature){
         var name = removeTag(removeDq(feature.properties.area_name)).toLowerCase();  
         var diabetesratio = feature.properties.hg_choles_me_2_rate_3_11_7_13;
         if(!lga_aus.includes(name)){
             lga_aus.push({"name":name, "diabetes":diabetesratio});
         } 
     });
//
     diabetes_aus.ratio = lga_aus;
//        
//    
     //LGA AREA OF VICTORIA
         $.each(VIC.features, function(i, feature){
             var collection = [];
             $.each(feature.geometry.coordinates[0][0], function(j, coordinate){
                 collection.push({lat:coordinate[1], lng:coordinate[0]});
             });
             drawSuburb(collection, removeTag(removeDq(feature.properties.vic_lga__3)).toLowerCase()); 

         });

     //LGA AREA OF QUEENSLAND
         $.each(QLD.features, function(i, feature){
             var collection = [];
             $.each(feature.geometry.coordinates[0][0], function(j, coordinate){
                 collection.push({lat:coordinate[1], lng:coordinate[0]});
             });
             drawSuburb(collection, removeTag(removeDq(feature.properties.qld_lga__3)).toLowerCase()); 
         });

     //LGA AREA OF NSW
         $.each(NSW.features, function(i, feature){
             var collection = [];
             $.each(feature.geometry.coordinates[0][0], function(j, coordinate){
                 collection.push({lat:coordinate[1], lng:coordinate[0]});
             });
             drawSuburb(collection, removeTag(removeDq(feature.properties.nsw_lga__3)).toLowerCase()); 
         });
    
     //LGA AREA OF SA
         $.each(SA.features, function(i, feature){
             var collection = [];
             $.each(feature.geometry.coordinates[0][0], function(j, coordinate){
                 collection.push({lat:coordinate[1], lng:coordinate[0]});
             });
             drawSuburb(collection, removeTag(removeDq(feature.properties.sa_lga_s_3)).toLowerCase()); 
         });
    
      //LGA AREA OF WA
         $.each(WA.features, function(i, feature){
             var collection = [];
             $.each(feature.geometry.coordinates[0][0], function(j, coordinate){
                 collection.push({lat:coordinate[1], lng:coordinate[0]});
             });
             drawSuburb(collection, removeTag(removeDq(feature.properties.wa_lga_s_3)).toLowerCase()); 
         });
    
     //LGA AREA OF TAS
         $.each(TAS.features, function(i, feature){
             var collection = [];
             $.each(feature.geometry.coordinates[0][0], function(j, coordinate){
                 collection.push({lat:coordinate[1], lng:coordinate[0]});
             });
             drawSuburb(collection, removeTag(removeDq(feature.properties.tas_lga__3)).toLowerCase()); 
         });
    
     //LGA AREA OF NT
         $.each(NT.features, function(i, feature){
             var collection = [];
             $.each(feature.geometry.coordinates[0][0], function(j, coordinate){
                 collection.push({lat:coordinate[1], lng:coordinate[0]});
             });
             drawSuburb(collection, removeTag(removeDq(feature.properties.nt_lga_s_3)).toLowerCase()); 
         });
    
     //LGA AREA OF OTHER
         $.each(OTHER.features, function(i, feature){
             var collection = [];
             $.each(feature.geometry.coordinates[0][0], function(j, coordinate){
                 collection.push({lat:coordinate[1], lng:coordinate[0]});
             });
             drawSuburb(collection, removeTag(removeDq(feature.properties.ot_lga_s_3)).toLowerCase()); 
         });
    });

function removeDq(name){
    return (name.replace(/['"]+/g, ''));
}

function removeTag(name){
    return (name.replace(/\s\(.*?\)/g, ''));
}
