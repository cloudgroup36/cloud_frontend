//change url to get dat from server
var url = '/apiList';
var pieChart1, pieChart2,pieChart3,pieChart4;
var barChart;
var dataAll=[];
var dataMel=[];
var dataSyd=[];
var dataBris=[];
var subTitles =['staple food', 'snack & dessert', 'drink', 'raw'];
var mealData = [];

$(document).ready(function(){
   loadPiechart();
   $.getJSON(url, function(data){
       $.each(data, function(i, json){
           var qid = json.qid;
           if(qid ==='q1'){
             $.each(json.data, function(i,item){
                 dataAll.push(item);
             });
           }else if(qid ==='q2'){
              var cities = json.cities;
               $.each(cities, function(i, item){
                   if(item.city==='Melbourne'){
                        $.each(item.data, function(i,d){
                            dataMel.push(d);
                        });
                   }else if (item.city==='Sydney'){
                        $.each(item.data, function(i,d){
                            dataSyd.push(d);
                        });
                   }else if(item.city==='Brisbane'){
                        $.each(item.data, function(i,d){
                            dataBris.push(d);
                        });
                   }
               });
           }
           
       
   });
});

});

function loadPiechart(){
    //                {
//            name:subTitles[0],
//            type:'pie',
//            radius : ['30%','50%'],
//            center: ['25%','35%'],
//            label: {
//                normal: {
//                    textStyle: {
//                        fontSize:16
//                    }
//                }
//            }
//        },
//        {
//            name:subTitles[1],
//            type:'pie',
//            radius : ['30%','50%'],
//            center: ['75%','35%'],
//            label: {
//                normal: {
//                    textStyle: {
//                        fontSize:16
//                    }
//                }
//            }
//        },
//        {
//            name:subTitles[2],
//            type:'pie',
//            radius : ['30%','50%'],
//            center: ['25%','95%'],
//            label: {
//                normal: {
//                    textStyle: {
//                        fontSize:16
//                    }
//                }
//            }
//        },
//        {
//            name:subTitles[3],
//            type:'pie',
//            radius : ['30%','50%'],
//            center: ['75%','95%'],
//            label: {
//                normal: {
//                    textStyle: {
//                        fontSize:16
//                    }
//                }
//            }
//        }
    pieChart1 = echarts.init(document.getElementById("chart1"));
    pieChart1.setOption({
    title : {
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    calculable : true,
    series : [
                        {
            name:subTitles[0],
            type:'pie',
            radius : ['30%','50%'],
            center: ['50%','40%'],
            label: {
                normal: {
                    textStyle: {
                        fontSize:16
                    }
                }
            }
        }
    ]
    });
    
    
    pieChart2 = echarts.init(document.getElementById("chart2"));
    pieChart2.setOption({
    title : {
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    calculable : true,
    series : [
                        {
            name:subTitles[1],
            type:'pie',
            radius : ['30%','50%'],
            center: ['50%','40%'],
            label: {
                normal: {
                    textStyle: {
                        fontSize:16
                    }
                }
            }
        }
    ]
    });
    
    pieChart3 = echarts.init(document.getElementById("chart3"));
    pieChart3.setOption({
    title : {
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    calculable : true,
    series : [
                        {
            name:subTitles[2],
            type:'pie',
            radius : ['30%','50%'],
            center: ['50%','40%'],
            label: {
                normal: {
                    textStyle: {
                        fontSize:16
                    }
                }
            }
        }
    ]
    });
    
    
    pieChart4 = echarts.init(document.getElementById("chart4"));
    pieChart4.setOption({
    title : {
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    calculable : true,
    series : [
                        {
            name:subTitles[3],
            type:'pie',
            radius : ['30%','50%'],
            center: ['50%','40%'],
            label: {
                normal: {
                    textStyle: {
                        fontSize:16
                    }
                }
            }
        }
    ]
    });
}
    

function loadData(title){
    var dataset;
    if(title==='All'){
        dataset = dataAll;
    }else if(title==='Melbourne'){
        dataset = dataMel;
    }else if(title==='Sydney'){
        dataset=dataSyd;
    }else if(title ==='Brisbane'){
        dataset=dataBris;
    }
    
    var series1 = [{data:loadResults(dataset,subTitles[0])}];
    pieChart1.setOption({series:series1,title:{text:"Staple Food"}});
    var series2 = [{data:loadResults(dataset,subTitles[1])}];
    pieChart2.setOption({series:series2,title:{text:"Snack & Dessert"}});
    var series3 = [{data:loadResults(dataset,subTitles[2])}];
    pieChart3.setOption({series:series3,title:{text:"Drink"}});
    var series4 = [{data:loadResults(dataset,subTitles[3])}];
    pieChart4.setOption({series:series4,title:{text:"Raw Food"}});
//    var series=
//        [{
//            data: loadResults(dataset,subTitles[0])
//        },
//        {
//            data: loadResults(dataset,subTitles[1])
//        },
//        {
//            data: loadResults(dataset,subTitles[2])
//        },
//        {
//            data:loadResults(dataset,subTitles[3])
//        }];
//    pieChart.setOption({series:series});
}
    
    
function loadResults(data,subtitle){
    var items=[];
    for (var i = 0;i<data.length;i++){
        console.log(subtitle);
        if(data[i].type===subtitle){
            for(var j = 0; j< data[i].result.length; j++){
                items.push({value:data[i].result[j].EmotionIndex, name:data[i].result[j].foodName});
                console.log(data[i].result[j].foodName);
            }     
        }
    }
    return items;
}




