//change url to get dat from server
var url = '/apiList';
var mealData=[];
$(document).ready(function(){
   
    loadBarChart();
    
    $.getJSON(url, function(data){
        $.each(data, function(i,json){
           if(json.qid==='q5'){
               
               $.each(json.data, function(i, meal){
                   if(i<4){
                     mealData.push(meal.score);
                    console.log("With score "+meal.score);
                   }

               });
           } 
        });
    });
});


function loadBarChart(){
    barChart = echarts.init(document.getElementById("bar"));
    barChart.setOption({
            tooltip: {},
            legend: {
                data:['Sentiment Score']
            },
            xAxis: {
                data: ["Brunch","Breakfast","Lunch","Dinner"]
            },
            yAxis: {},
            series: [{
                name: 'Sentiment Score',
                type: 'bar',
                data: mealData
            }]
        
    });
}